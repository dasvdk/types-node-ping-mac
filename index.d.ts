import { IPingResult } from 'node-scan-interfaces';
export declare class Arp {
    constructor();
    getIps(mac: string): Promise<IpResult[]>;
    isAlive(mac: string): Promise<IPingResult>;
}
export interface IpResult {
    ip: string;
}
